package org.davinci.aspectouno;

import java.util.Scanner;

public class MethodisEqualToTest {
    private Scanner scanner;

    public MethodisEqualToTest() {
        scanner = new Scanner(System.in);
    }

    public static <T> boolean isEqualTo(T first, T second) {
        return first.equals(second);
    }

    public void testIsEqualTo() {
        Integer a;
        Integer b;

        System.out.print("Enter two integer values: ");
        a = scanner.nextInt();
        b = scanner.nextInt();
        System.out.printf("%d and %d are %s\n", a, b,
                (isEqualTo(a, b) ? "equal" : "not equal"));

        String c;
        String d;

        System.out.print("\nEnter two string values: ");
        c = scanner.next();
        d = scanner.next();
        System.out.printf("%s and %s are %s\n", c, d,
                (isEqualTo(c, d) ? "equal" : "not equal"));

        Double e;
        Double f;

        System.out.print("\nEnter two double values: ");
        e = scanner.nextDouble();
        f = scanner.nextDouble();
        System.out.printf("%.1f and %.1f are %s\n", e, f,
                (isEqualTo(e, f) ? "equal" : "not equal"));

        Object g = new Object();
        Object h = new Object();

        System.out.printf("\n%s and %s are %s\n", g, h,
                 (isEqualTo(g, h) ? "equal" : "not equal") );
    }

    public static void main(String args[]) {
        MethodisEqualToTest application = new MethodisEqualToTest();
        application.testIsEqualTo();
    }
}
