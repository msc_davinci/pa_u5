package org.davinci.aspectodos;

public class PairTest {
    public static void main(String args[]) {
        Pair<Integer, String> numberPair =
                new Pair<Integer, String>(1, "one");


        System.out.printf("Original pair: < %d, %s >\n",
                numberPair.getFirst(), numberPair.getSecond());

        numberPair.setFirst(2);
        numberPair.setSecond("Second");

        System.out.printf("Modified pair: < %d, %s >\n",
                numberPair.getFirst(), numberPair.getSecond());
    }
}